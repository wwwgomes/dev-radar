const { Router } = require('express');
const DevController = require('./controllers/DevController');
const SearchController = require('./controllers/SearchController');

const router = Router();

router
  .route('/devs')
  .get(DevController.index)
  .post(DevController.store);

router.get('/search', SearchController.index);

module.exports = router;
