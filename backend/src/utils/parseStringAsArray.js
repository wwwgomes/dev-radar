module.exports = {
  parseStringAsArray: stringAsArray => {
    return stringAsArray.split(',').map(o => o.trim());
  },
};
